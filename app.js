const errorHelper = require('./Helpers/ErrorHelpers/ErrorHelpers');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const helmet = require('helmet');

const config = require('./config.json');

let app = express();

app.use(helmet());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

const mainDatabaseHelper = require('./Helpers/DatabaseHelpers/MainDatabaseHelper');
(async () => {
	
	const env = app.get('env');
	const conn = await mainDatabaseHelper.connect(config[env].database.url);
	await mainDatabaseHelper.initializeAutoIncrementPlugin(conn);
	
	/**
	 * Add routes.
	 */
	app.use('/users', require('./routes/v1/UserRoutes'));
	app.use('/books', require('./routes/v1/BookRoutes'));
	
	/**
	 * Error handling.
	 */
	app.use(errorHelper.notFoundError);
	app.use(errorHelper.error);
})();

module.exports = app;
