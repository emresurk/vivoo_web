const mongoose = require('mongoose');
const autoIncrement = require('mongoose-auto-increment');

const mainDatabaseHelper = {};

mainDatabaseHelper.connect = async function (url) {
	const conn = await mongoose.connect(
		url,
		{
			useNewUrlParser: true,
			useCreateIndex : true,
			useUnifiedTopology: true
		}
	);
	//console.log("Mongo db connected");
	return conn;
};

mainDatabaseHelper.initializeAutoIncrementPlugin = async function (conn) {
	await autoIncrement.initialize(conn);
};
module.exports = mainDatabaseHelper;