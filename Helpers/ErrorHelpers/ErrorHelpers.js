const createError = require('http-errors');

const errorHelper = {};

errorHelper.notFoundError = function (req, res, next) {
	next(createError(404));
};

errorHelper.error = function (err,req,res,next) {
	const errObj = {
		message : err.message
	};
	if ( req.app.get('env') === 'development' ) {
		errObj.error = err;
	}
	res.status(err.status || 500);
	res.json(errObj);
};

module.exports = errorHelper;