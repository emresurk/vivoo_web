const userController = {};
const UserModal = require('./../Modals/UserModal');
const userServices = require('./../Services/UserServices');
const bookServices = require('./../Services/BookServices');
const borrowArchiveServices = require('../Services/BorrowArchiveServices');
const responseObjects = require('../Helpers/ResponseHelpers/ResponseObjects');

userController.getAllUsers = async function (req, res) {
	const users = await userServices.getAllUsers();//await UserModal.find();
	res.json(users);
};

userController.getUser = async function (req, res) {
	const userId = req.params.userId;
	let user;
	
	if (userId === undefined) {
		return res.status(400).end();
	}
	
	user = await userServices.getUserById(userId);
	
	if (user === null) {
		return res.status(404).end();
	}
	
	res.json(user);
};

userController.createUser = async function (req, res) {
	const postObj = req.body;
	const user = new UserModal(postObj);
	user.validateSync();
	if (user.errors) {
		return res.status(400).json(user.errors);
	}
	
	const createdUser = await userServices.createUser(user);
	
	res.json(createdUser);
};

userController.borrowBook = async function (req, res) {
	const userId = req.params.userId;
	const bookId = req.params.bookId;
	
	if (
		userId === undefined ||
		bookId === undefined
	) {
		return res.status(400).end();
	}
	
	const book = await bookServices.getBookById(bookId);
	if (book === null ) {
		return res.status(404).end();
	}
	
	if ( book.borrowedUser !== undefined ) {
		if (book.borrowedUser.id == userId ) {
			return res.status(418).json( responseObjects.message('That user already keeps that book.'));
		} else {
			return res.status(418).json(responseObjects.message('Another user keeps that book, sorry.'));
		}
	}
	
	book.borrowedUser = await userServices.getUserById(userId);
	book.save();
	res.json(book);
	
};

userController.returnBook = async function (req, res) {
	const userId = req.params.userId;
	const bookId = req.params.bookId;
	const score = req.body.score;
	
	if (
		userId === undefined ||
		bookId === undefined ||
		score === undefined
	) {
		return res.status(400).end();
	}
	
	const book = await bookServices.getBookById(bookId);
	if ( book.borrowedUser === undefined ){
		return res.status(418).json( responseObjects.message("No one borrowed that book." ));
	}
	console.log(" b: " , book );
	
	borrowArchiveServices.addReturningArchive(
		book.borrowedUser._id,
		book._id,
		score
	);
	
	book.borrowedUser = undefined;
	book.save();
	
	res.json({});
};


module.exports = userController;
