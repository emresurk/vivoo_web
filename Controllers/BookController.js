const bookService = require('./../Services/BookServices');
const BookModal  =require('./../Modals/BookModal');
const bookController = {};

bookController.getAllBooks = async function (req, res) {
	res.json(await bookService.getAllBooks());
};

bookController.getBookById = async function (req, res) {
	const bookId = req.params.bookId;
	const book = await bookService.getBookById(bookId);
	if ( book === null ) {
		return res.status(404).end();
	}
	res.json(book);
};

bookController.createBook = async function (req, res) {
	const book = new BookModal(req.body);
	book.validateSync();
	if(book.errors){
		return res.status(400).json(book.errors);
	}
	const created = await bookService.createBook(book);
	res.json(created);
};

module.exports = bookController;