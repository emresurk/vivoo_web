const BorrowArciveModal = require('./../Modals/BorrowArciveModal');

const borrowArchiveServices = {};

borrowArchiveServices.addReturningArchive = async function (
	userObjectId,
	bookObjectId,
	score
) {
	const bookArchive = new BorrowArciveModal({
		user : userObjectId,
		book : bookObjectId,
		returnDate : new Date(),
		score : score
	});
	return bookArchive.save();
};

module.exports = borrowArchiveServices ;