const UserModal = require('./../Modals/UserModal');


const userServices = {};

/**
 * @returns {Promise<UserModal>}
 */
userServices.getAllUsers = async function () {
	return await UserModal.find();
};

/**
 * @param userId
 * @returns {Promise<UserModal>}
 * @desc returns user if found, returns null otherwise.
 */
userServices.getUserById = async function (userId) {
	return await UserModal.findOne({ id : userId  });
};


/**
 * @param userSchema
 * @returns {Promise<UserModal>}
 */
userServices.createUser = async function (userSchema) {
	return await userSchema.save();
};

module.exports = userServices;
