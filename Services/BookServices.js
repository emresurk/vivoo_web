const BookModal = require('./../Modals/BookModal');

const bookServices = {};

/**
 * @returns {Promise<UserModal>}
 */
bookServices.getAllBooks = async function () {
	return await BookModal.find().populate('borrowedUser');
};

/**
 * @param bookId
 * @returns {Promise<BookModal>}
 * @desc returns user if found, returns null otherwise.
 */
bookServices.getBookById = async function (bookId) {
	return await BookModal.findOne({ id : bookId  }).populate('borrowedUser');
};

/**
 * @param bookModal
 * @returns {Promise<BookModal>}
 */
bookServices.createBook = async function (bookModal) {
	return await bookModal.save();
};

module.exports = bookServices;
