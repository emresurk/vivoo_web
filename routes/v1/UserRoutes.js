const express = require('express');
const router = express.Router();
const userController = require('../../Controllers/UserController');

router.get('/', userController.getAllUsers);
router.get('/:userId', userController.getUser);
router.post('/', userController.createUser);
router.post('/:userId/borrow/:bookId', userController.borrowBook);
router.post('/:userId/return/:bookId', userController.returnBook);

module.exports = router;
