const express = require('express');
const router = express.Router();
const bookController = require('./../../Controllers/BookController');

router.get('/', bookController.getAllBooks );
router.get('/:bookId', bookController.getBookById);
router.post('/',bookController.createBook );

module.exports = router;