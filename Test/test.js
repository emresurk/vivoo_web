var expect = require('chai').expect;
var request = require('request');
const app = require('./../bin/vivoo');

it('Main page content', function (done) {
	request('http://localhost:3000', function (error, response, body) {
		expect(response.statusCode).to.equal(404);
		done();
	});
});

it('Should create user.', function (done) {
	request(
		'http://localhost:3000/users',
		{username: 'TestUser'},
		function (
			error,
			response,
			body
		) {
			expect(response.statusCode).to.equal(200);
			done();
		});
});