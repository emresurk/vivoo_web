const mongoose = require('mongoose');

const borrowArchiveSchema = mongoose.Schema({
	user: {
		type: mongoose.Types.ObjectId,
		ref: 'Users'
	},
	book : {
		type : mongoose.Types.ObjectId,
		ref : 'Books'
	},
	returnDate : Date,
	score : Number
});

module.exports =
	mongoose.model(
		'BorrowArchives',
		borrowArchiveSchema
	);
