const mongoose = require('mongoose');
const autoIncrement = require('mongoose-auto-increment');

const userSchema = new mongoose.Schema(
	{
		name: {
			type: String,
			unique: false,
			required: [
				true,
				"Username is required."
			]
		}
	}
);

userSchema.plugin(
	autoIncrement.plugin,
	{
		model: 'Users',
		field: 'id',
		startAt : 1
	}
);

module.exports =
	mongoose.model(
		'Users',
		userSchema
	);

