const mongoose = require('mongoose');
const autoIncrement = require('mongoose-auto-increment');

const bookSchema = new mongoose.Schema(
	{
		name: {
			type: String,
			unique: false,
			required: [
				true,
				"Book name is required."
			]
		},
		borrowedUser: {
			type: mongoose.Schema.Types.ObjectId,
			ref: 'Users'
		}
	}
);

bookSchema.plugin(
	autoIncrement.plugin,
	{
		model: 'Books',
		field: 'id',
		startAt: 1
	}
);

module.exports =
	mongoose.model(
		'Books',
		bookSchema
	);

